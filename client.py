#!/usr/bin/env python2

import socket
import logging
import random
import threading
import signal
import sys

from base64 import b64encode
from time import sleep


class Manager(object):
    """
    Class representing main thread of client
    """
    def __init__(self):
        signal.signal(signal.SIGINT, self.signal_handler)

    def new_thread(self, port=9999):
        """
        Creates and returns new thread
        """
        srv = DockerServer(parent=self, port=port)
        srv.daemon = True
        srv.start()
        return srv

    def process_data(self, data):
        """
        Callback for data processing
        """
        sleep(random.uniform(0, 2))
        return b64encode(data)   

    def signal_handler(self, signal, frame):
        sys.exit(0)


class DockerServer(threading.Thread):
    """
    Class representing listener in containers
    """
    def __init__(self, parent, port):
        self.parent = parent
        self.sock = socket.socket()
        self.port = port
        self._setup_logging()
        super(DockerServer, self).__init__()

    def _setup_logging(self):
        """
        Setup logging to a file
        """
        fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        logging.basicConfig(filename='/var/log/ya-docker.log', 
                            level=logging.DEBUG, format=fmt)

    def run(self):
        """
        Listener thread
        """
        self.sock.bind(('', self.port))
        self.sock.listen(5)
        while True:
            conn, addr = self.sock.accept()
            logging.debug('Accepted connection from %s' % addr[0])
            conn.settimeout(60)

            data = ''
            while not data:
                data = conn.recv(1024)
                sleep(0.1)
            logging.info('Got data %s' % data)

            new_data = self.parent.process_data(data)
            conn.send(new_data)
            logging.info('Sent data %s' % new_data)

            conn.close()
            logging.debug('Closed connection from %s' % addr[0])

if __name__ == '__main__': 
    mgr = Manager()
    th = mgr.new_thread()
    while th.isAlive():
        sleep(1)


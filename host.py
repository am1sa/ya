#!/usr/bin/env python2

import subprocess
import threading
import os
import socket
import string
import random
import logging
import signal
import sys

from time import sleep

DOCKER_BIN = "/usr/bin/docker"
IMAGE_NAME = "ubuntu-ya"


class Util(object):
    """
    Class with a few helpful methods
    """
    @staticmethod
    def generate_msg(msg_size=32):
        """
        Returns randomized message of lowercase symbols and digits
        """
        return ''.join(random.choice(string.ascii_lowercase + 
                       string.digits) for x in range(msg_size))

    @staticmethod
    def setup_logging():
        """
        Configures logging
        """
        fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        logging.basicConfig(filename='/var/log/yandex-host.log', 
                            level=logging.DEBUG, format=fmt)

    @staticmethod
    def cleanup(err):
        """
        Cleanup function on error.
        Prints error to stderr, logs it to log file and exits.
        """
        sys.stderr.write(err + '\n')
        logging.error(err)
        sys.exit(-1)


class Manager(object):
    """
    Class representing main thread of host
    """
    def __init__(self):
        signal.signal(signal.SIGINT, self.signal_handler)

    def signal_handler(self, signal, frame):
        sys.exit(0)

    def new_thread(self, port=9999):
        """
        Function creates and returns new thread
        """
        srv = Docker(parent=self, port=port)
        srv.daemon = True
        srv.start()
        return srv

    def send_data(self, ctid, ip, port, first_try=True):
        """
        Callback function - sends data to a container.
        """
        sock = socket.socket()
        try:
            sock.connect((ip, port))
            logging.info('Connection to %s:%d successful!' % (ip, port))
        except socket.error:
            # container - may not be yet up: retry for the first time
            if first_try:
                sleep(2)
                send_data(self, ctid, ip, port, False)
                return
            else:
                Util.cleanup('Unable to connect to CT %s, IP %s' % (ctid, ip))

        msg = Util.generate_msg()
        sock.send(Util.generate_msg())
        logging.info('Sent message: %s to %s:%d' % (msg, ip, port))

        reply = sock.recv(1024)
        logging.info('Got reply: %s to message %s from %s:%d' % 
                     (reply, msg, ip, port))

        sock.close()
        logging.debug('Closed connection to %s:%d' % (ip, port))


class Docker(threading.Thread):
    """
    Class for container and images management.
    """
    def __init__(self, parent, port):
        Util.setup_logging()
        self._check_env_setup()
        self.id, self.ip = self.create_container()
        self.port = port
        self.parent = parent
        super(Docker, self).__init__()

    def build_image(self):
        """
        Build docker image from Dockerfile.
        """
        logging.debug('Started image building...')
        if subprocess.Popen([DOCKER_BIN, 'build', '-t', IMAGE_NAME,
                            '/root/ya'], stdout=open(os.devnull, 'w')).wait():
            Util.cleanup('Build docker image failed. Code %d' % p.returncode)
        logging.info('Build image success!')
        
    def _check_env_setup(self):
        """
        Check if docker image exists.
        """
        if not os.path.exists(DOCKER_BIN):
            Util.cleanup('Docker binary %s not found' % DOCKER_BIN)
        p = subprocess.Popen([DOCKER_BIN, 'images', '-q', IMAGE_NAME],
                             stdout=subprocess.PIPE)
        if not p.communicate()[0]:
            self.build_image()

    def create_container(self):
        """
        Create docker container.
        """
        logging.debug('Started container creation...')
        p = subprocess.Popen([DOCKER_BIN, 'run', '-d', '-t', IMAGE_NAME],
                             stdout=subprocess.PIPE)
        ctid = p.communicate()[0].strip()
        if not ctid or p.returncode:
            Util.cleanup('Container creation failed. Code %d' % p.returncode)
        logging.info('Container creation successful, id = %s!' % ctid)

        logging.debug('Getting container\'s ip address...')
        p = subprocess.Popen([DOCKER_BIN, 'inspect', '--format',
                             '{{ .NetworkSettings.IPAddress }}', ctid],
                             stdout=subprocess.PIPE)
        ip = p.communicate()[0].strip()
        if not ip or p.returncode:
            Util.cleanup('Unable to get IP address of the CT %s, code: %d' %
                         (ctid, p.returncode))
        logging.debug('Got container\'s IP address: %s' % ip)
        return ctid, ip

    def run(self):
        """
        Emulate data send requests.
        """
        while True:
            sleep(random.uniform(0, 2))
            self.parent.send_data(self.id, self.ip, self.port)


if __name__ == '__main__':
    mgr = Manager()
    th = mgr.new_thread()
    while th.isAlive():
        sleep(1)


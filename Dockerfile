FROM ubuntu:16.04
MAINTAINER Inga Pyass (inga.pyass@gmail.com)
RUN apt-get update
RUN apt-get install python -y
RUN mkdir -p /ya
COPY client.py /ya
CMD /ya/client.py
